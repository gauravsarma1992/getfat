var getfat = angular.module('getfat', ['ngAutocomplete', 'templates', 'ui.bootstrap', 'ui.router', 'toastr']);

//getfat.directive('ngEnter', function() {
//  return function(scope, element, attrs) {
//      element.bind("keydown keypress", function(event) {
//          if(event.which === 13) {
//              scope.$apply(function(){
//                  scope.$eval(attrs.ngEnter, {'event': event});
//              });
//
//              event.preventDefault();
//          }
//      });
//  };
//});

getfat.config(function($stateProvider, $urlRouterProvider, $uiViewScrollProvider, SearchRoutes) {

    $uiViewScrollProvider.useAnchorScroll();
    $urlRouterProvider.otherwise('/home/dashboard');

    SearchRoutes($urlRouterProvider, $stateProvider);

    //$stateProvider

    //.state('home', {
    //    url: '/home',
    //    templateUrl: 'home.html',
    //})

    //.state('home.dashboard', {
    //    url: '/dashboard',
    //    templateUrl: 'dashboard.html',
    //})


});
